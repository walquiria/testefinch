package Teste;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Finch {
	private Document doc;

	public Finch(Document doc) {
		this.doc = doc;
	}
	
	
	public static void main(String[]args){
	try {
		Document document = Jsoup.connect("https://globoesporte.globo.com/placar-ge/hoje/jogos.ghtml").get();
		Finch teste = new Finch(document);
		teste.getGames();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

	private void getGames() {
		Elements mandante = doc.getElementsByClass("time mandante small-8 medium-10");
		for(Element element : mandante){
			String time_mandante = element.getElementsByTag("abbr").attr("title");
			System.out.println("time mandante: " + time_mandante);
		}
		
		Elements visitante = doc.getElementsByClass("time visitante small-8 medium-10");
		for(Element element : visitante){
			String time_visitante = element.getElementsByTag("abbr").attr("title");
			System.out.println("time visitante: " + time_visitante);
		}
		
		Elements horario = doc.getElementsByClass("hora-local");
		for(Element element : horario){
			String hora = element.text();
			System.out.println("horario: " + hora);
		}
			
	}

	}
